import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { VideoResource } from './interface/videoResource';
import { Videos } from './interface/videos';

@Injectable({
  providedIn: 'root'
})
export class VidoServiceService {

  const baseUrl = "https://youtube.googleapis.com/youtube/v3/";
  const apikey = "AIzaSyAtt9FBOZ25pdHfAi7xH_J6R_dw7mYDLDs";

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  research(keyword: string, order: string): Observable<VideoResource>{
    // search a list of video according to a keyword 
    if(!keyword.trim()){
      return of();
    }
    const baseRequest = this.baseUrl+`search?part=snippet&maxResults=25&q=${keyword}&key=${this.apikey}`;
    //add a parameter order if it's exist
    const request = !order.trim()?baseRequest: baseRequest+`&order=${order}`;
    return this.http.get<VideoResource>(request).pipe(
      /*if no result => not found */
      catchError(this.handler<VideoResource>('research'))
    )
  }

  getVideosById(id: string): Observable<VideoResource>{
    /* search a list of videos id */
    const request = this.baseUrl+`videos?part=snippet&id=${id}&key=${this.apikey}`;
    return this.http.get<VideoResource>(request).pipe(
      /*if no result => not found */
      catchError(this.handler<VideoResource>('getVideosById'))
    )
  }

  getPopularVideo(region = 'US'): Observable<Videos>{
    //get most popular video by region, region is set to US by default
    const request = this.baseUrl+`videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&regionCode=${region}&key=${this.apikey}`;
    return this.http.get<Videos>(request).pipe(
      catchError(this.handler<Videos>('getPopularVideos'))
    )
  }

  private handler<E>(operation = 'operation', exeptedResult?: E) {
    /* return a handler according to the opperation and exepted result*/ 
    return (error: any): Observable<E> => {
      console.error(error);
      this.updateErrorService('research error');
      return of(exeptedResult as E);//return the expted result
    };
  }

  private updateErrorService(message: string):void{
    // TODO: update the message service (so the message componnet can print the error message)
  }
}
