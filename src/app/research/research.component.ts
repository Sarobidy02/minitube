import { Component, OnInit } from '@angular/core';
import { ResearchResponse } from '../interface/researchResponse';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.css']
})
export class ResearchComponent implements OnInit {
  videos?: ResearchResponse;
  constructor() { }

  ngOnInit(): void {
  }

}
