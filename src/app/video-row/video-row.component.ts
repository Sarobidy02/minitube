import { Component, OnInit } from '@angular/core';
import { RowOfVideoResource } from '../interface/rowOfVideoResource';
@Component({
  selector: 'app-video-row',
  templateUrl: './video-row.component.html',
  styleUrls: ['./video-row.component.css']
})
export class VideoRowComponent implements OnInit {
  line?: RowOfVideoResource;
  constructor() { }

  ngOnInit(): void {
  }

}
