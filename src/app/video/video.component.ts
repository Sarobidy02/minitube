import { Component, OnInit } from '@angular/core';
import { RowOfVideoResource } from '../interface/rowOfVideoResource';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  videos: RowOfVideoResource[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}
