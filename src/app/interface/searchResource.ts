/*the video information contained in the searchResource for each video*/
export interface SearchResource{
  kind: string;
  etag: string;
  id: {
    kind: string;
    videoId: string;
    channelId: string;
    playlistId: string;
  };
  snippet: {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: {
      default: {
        url: string;
        width:  number;
        height:  number;
      };
      medium: {
        url: string;
        width:  number;
        height:  number;
      };
      high: {
        url: string;
        width:  number;
        height:  number;
      };
    },
    channelTitle: string;
    liveBroadcastContent: string;
    publishTime: string;
  }
}
