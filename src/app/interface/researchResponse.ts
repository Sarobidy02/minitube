import { SearchResource } from './searchResource';
/*response to research request*/
export interface ResearchResponse{
  kind: string;
  etag: string;
  nextPageToken: string;
  prevPageToken: string;
  regionCode: string;
  pageInfo: {
    totalResults: number;
    resultsPerPage: number;
  };
  items: SearchResource []
}