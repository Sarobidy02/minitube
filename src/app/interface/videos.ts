import { VideoResource } from './videoResource';
/*videos list ex: list of most popular video*/
export interface Videos{
  kind: string;
  etag: string;
  nextPageToken: string;
  prevPageToken: string;
  pageInfo: {
    totalResults: number;
    resultsPerPage: number;
  },
  items: VideoResource[]
}