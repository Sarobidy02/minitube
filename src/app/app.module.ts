import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VideoComponent } from './video/video.component';
import { ResearchComponent } from './research/research.component';

import { HomeComponent } from './home/home.component';
import { VideoRowComponent } from './video-row/video-row.component';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import { SelectedVideoComponent } from './selected-video/selected-video.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    VideoComponent,
    ResearchComponent,

    HomeComponent,
    VideoRowComponent,
    VideoDetailComponent,
    SelectedVideoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
