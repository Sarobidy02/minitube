import { Component, OnInit } from '@angular/core';
import { VideoResource } from '../interface/videoResource';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.css']
})
export class VideoDetailComponent implements OnInit {
  video?: VideoResource;
  constructor() { }

  ngOnInit(): void {
  }

}
