import { TestBed } from '@angular/core/testing';

import { VidoServiceService } from './vido-service.service';

describe('VidoServiceService', () => {
  let service: VidoServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VidoServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
