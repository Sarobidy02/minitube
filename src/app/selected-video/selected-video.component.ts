import { Component, Input, OnInit } from '@angular/core';
import { VideoResource } from '../interface/videoResource';

@Component({
  selector: 'app-selected-video',
  templateUrl: './selected-video.component.html',
  styleUrls: ['./selected-video.component.css']
})
export class SelectedVideoComponent implements OnInit {
  @Input() selected?: VideoResource;
  constructor() { }

  ngOnInit(): void {
  }

}
